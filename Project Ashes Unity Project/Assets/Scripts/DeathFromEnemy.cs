﻿using UnityEngine;
using System.Collections;

public class DeathFromEnemy : MonoBehaviour {

    // Detects if the enemy hit the player, if so then Player dies
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Player") {
            Debug.Log("Player Dies");
        }
    }
}
