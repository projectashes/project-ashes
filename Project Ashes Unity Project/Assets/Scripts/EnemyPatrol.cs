﻿using UnityEngine;
using System.Collections;

public class EnemyPatrol : MonoBehaviour {
    
    public float speed;         // Walking speed for the Enemy
    public float bounceAmount;  // How high the Player bounces off the enemies' head
    public LayerMask enemyMask; // Linecase ONLY detects these layers

    // These values modify how long the isBlocked Linecast is.
    // Make sure this linecast never touches the ground
    public float isBlockedTopPoint;
    public float isBlockedBottomPoint;

    // These values modify how long the isGrounded Linecast is.
    // Make sure this linecast streches past the ground where the enemy is standing
    public float isGroundedBottomPoint;

    [HideInInspector]
    public bool patrolMode = true;  // Detects if your chasing player

    
    private Transform myTrans;
    private Vector3 currRotation;
    private Rigidbody2D myBody;
    private Vector3 myVel;
    private GameObject player;
    private float myWidth, myHeight;
    private bool isKlowd;           // Detects if the enemy is a Klowd

    void Start () {

        // Get Transform, Rotation, Rigidbody, and Velocity
        myTrans = this.transform;
        currRotation = myTrans.eulerAngles;
        myBody = this.GetComponent<Rigidbody2D>();
        myVel = myBody.velocity;

        // Get width and height of Sprite
        SpriteRenderer mySprite = this.GetComponent<SpriteRenderer>();
        myWidth = mySprite.bounds.extents.x;
        myHeight = mySprite.bounds.extents.y;

        // Find Player GameObject
        player = GameObject.FindGameObjectWithTag("Player");

        // Checks to see if this enemy is a Klowd
        if (this.gameObject.name.Contains("Klowd")) {
            isKlowd = true;
            Debug.Log("This is a Klowd");
        };
    }

    // Linecast & Movement
    void FixedUpdate () {

        // Linecast are used for the enemies to detect the ground, walls, and other enemies

        // The isGrounded Linecast is a line that starts infront of the enemy and extends slightly below the ground
        // This detects if there is no ground infront of them. If there is no ground then it turns the enemy around

        // The isBlocked Linecast is a line that starts at the top of the enemies head and extends to just above their feet
        // This detects if there is a wall or another enemy infront of them. If there is then it turns the enemy around

        //This is the start point for the isGrounded Linecast
        Vector2 lineCastPosCenter = toVector2(myTrans.position) - toVector2(myTrans.right) * myWidth;

        //This is the start point for the isBlocked Linecast (Sits infront of the enemies face)
        Vector2 lineCastPosTop = toVector2(myTrans.position) - toVector2(myTrans.right) * (myWidth + 0.1f) + Vector2.up * isBlockedTopPoint;
        Vector2 lineCastPosBottom = toVector2(myTrans.position) - toVector2(myTrans.right) * (myWidth + 0.1f) + Vector2.down * isBlockedBottomPoint;
        
        //Check to see if there's ground in front of you
        Debug.DrawLine(lineCastPosCenter, lineCastPosCenter + Vector2.down * isGroundedBottomPoint);    // Lets you see the linecast in Scene editor (White line)
        bool isGrounded = Physics2D.Linecast(lineCastPosCenter, lineCastPosCenter + Vector2.down * isGroundedBottomPoint, enemyMask);

        //Check to see if there's a wall in front of you
        Debug.DrawLine(lineCastPosTop, lineCastPosBottom, Color.red);   // Lets you see the linecast in Scene editor (Red line)
        bool isBlocked = Physics2D.Linecast(lineCastPosTop, lineCastPosBottom, enemyMask);
        
        // Checks to see if your a Klowd (Klowds don't check for ground)
        if (!isKlowd) {
            // If there is no ground or you hit a wall, turn around
            if (!isGrounded || isBlocked) {
                currRotation.y += 180;
                myTrans.eulerAngles = currRotation;
            }
        } else {
            // If you hit a wall, turn around
            if (isBlocked) {
                currRotation.y += 180;
                myTrans.eulerAngles = currRotation;
            }
        }

        // Checks to see if player is within sight
        if (patrolMode) {
            // If there is no player, continue to move forward
            myVel.x = -myTrans.right.x * speed;
        } else {
            // If there is a player, stop moving forward (The enemy AI script follows player)
            myVel.x = 0.1f;
        }
        myBody.velocity = myVel;
    }

    // Helper method to convert Vector3 to Vector2
    Vector2 toVector2(Vector3 vec) {
        return new Vector2(vec.x, vec.y);
    }

    public void Die() {
        // Add velocity to the Player so he bounces off enemy
        Rigidbody2D playerRigidbody = player.GetComponent<Rigidbody2D>();
        playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, bounceAmount);

        // Destroy enemy object
        Destroy(this.gameObject);
    }
}
