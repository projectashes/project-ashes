﻿using UnityEngine;
using System.Collections;

public class StiltsAI : MonoBehaviour {

    public float chaseSpeed;    // How fast the enemy goes once he sees the player

    private GameObject player;           // The Player
    private SpriteRenderer mySprite;    // Reference to Parent sprite renderer

    void Start() {
        mySprite = GetComponentInParent<SpriteRenderer>();
    }

    void OnTriggerStay2D(Collider2D col) {

        // If Player is within our line of sight chase after him
        if (col.gameObject.tag == "Player") {
            Debug.Log(col.gameObject.name + " is within sight");
            player = col.gameObject;
            if (player.transform.position.x < transform.position.x) {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(player.transform.position.x + mySprite.bounds.extents.x, transform.position.y, player.transform.position.z), Time.deltaTime * chaseSpeed);
            } else {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(player.transform.position.x - mySprite.bounds.extents.x, transform.position.y, player.transform.position.z), Time.deltaTime * chaseSpeed);
            }
            
        }
    }

    void OnTriggerExit2D(Collider2D col) {

        // If Player is out of our line of sight then go back to normal speed
        if (col.gameObject.tag == "Player") {
            Debug.Log(col.gameObject.name + " is out of sight");
            player = null;
        }
    }
}
