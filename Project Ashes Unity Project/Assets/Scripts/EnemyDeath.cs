﻿using UnityEngine;
using System.Collections;

public class EnemyDeath : MonoBehaviour {

    // Detects if the player jumped on the enemy, if so then Enemy dies
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Player") {
            EnemyPatrol enemy = transform.GetComponentInParent<EnemyPatrol>();
            enemy.Die();
        }
    }
}
