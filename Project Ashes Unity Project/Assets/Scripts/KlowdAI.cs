﻿using UnityEngine;
using System.Collections;

public class KlowdAI : MonoBehaviour {

    public float chaseSpeed;    // How fast the enemy goes once he sees the player
    
    private GameObject player;          // Reference to the Player
    private Transform myTrans;          // Reference to Parent Transform
    private Vector3 currentRotation;    // Reference to Parent Transform Rotation
    private SpriteRenderer mySprite;    // Reference to Parent Sprite Renderer
    private EnemyPatrol patrol;         // Reference to Patrol Script

    void Start() {
        // Set up initial variables
        player = GameObject.FindGameObjectWithTag("Player");
        myTrans = GetComponent<Transform>();
        currentRotation = myTrans.eulerAngles;
        mySprite = GetComponentInParent<SpriteRenderer>();
        patrol = GetComponent<EnemyPatrol>();
    }

    void OnTriggerEnter2D(Collider2D col) {
        // If Player is in our line of sight then stop patrolling
        if (col.gameObject.tag == "Player") {
            patrol.patrolMode = false;
        }
    }

    void OnTriggerStay2D(Collider2D col) {
        // Check if Player is within our line of sight
        if (col.gameObject.tag == "Player") {

            Debug.Log(col.gameObject.name + " is within sight");

            // Move towards our player
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, chaseSpeed * Time.deltaTime);

            // Face the direction of the player when chasing him
            if (player.transform.position.x > transform.position.x) {
                currentRotation.y = 180;    // Right
            } else {
                currentRotation.y = 0;      // Left
            }
            myTrans.eulerAngles = currentRotation;
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        // If Player is out of our line of sight then go back to patrolling
        if (col.gameObject.tag == "Player") {
            Debug.Log(col.gameObject.name + " is out of sight");
            patrol.patrolMode = true;
        }
    }
}
